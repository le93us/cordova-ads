// https://github.com/ratson/cordova-plugin-admob-free/tree/master/examples

class App {
  constructor(admobIdsArg) {
    this.admobIds = admobIdsArg
    App.initializeButtons(this.admobIds)
    App.setRewardAddsBehaviour()
    App.setInterstitialAddsBehaviour()
  }

  static initializeButtons(admobIds) {
    document.addEventListener(
      'deviceready',
      () => {
        admob.interstitial.config({
          id: admobIds.interstitial,
          isTesting: false,
          autoShow: false,
        })
        admob.interstitial.prepare()

        admob.rewardvideo.config({
          id: admobIds.rewardvideo,
          isTesting: false,
          autoShow: false,
        })
        admob.rewardvideo.prepare()

        document.getElementById('interstitialAds').disabled = true
        document.getElementById('interstitialAds').onclick = () =>
          admob.interstitial.show()

        document.getElementById('rewardVideoAds').disabled = true
        document.getElementById('rewardVideoAds').onclick = () =>
          admob.rewardvideo.show()
      },
      false,
    )
  }

  static setRewardAddsBehaviour() {
    document.addEventListener('admob.rewardvideo.events.LOAD_FAIL', (event) =>
      console.log(event),
    )

    document.addEventListener('admob.rewardvideo.events.LOAD', (event) => {
      console.log(event)
      document.getElementById('rewardVideoAds').disabled = false
    })

    document.addEventListener('admob.rewardvideo.events.CLOSE', (event) => {
      console.log(event)
      admob.rewardvideo.prepare()
    })
  }

  static setInterstitialAddsBehaviour() {
    document.addEventListener('admob.interstitial.events.LOAD_FAIL', (event) =>
      console.log(event),
    )

    document.addEventListener('admob.interstitial.events.LOAD', (event) => {
      console.log(event)
      document.getElementById('interstitialAds').disabled = false
    })

    document.addEventListener('admob.interstitial.events.CLOSE', (event) => {
      console.log(event)
      admob.interstitial.prepare()
    })
  }
}

const admobIds = {
  interstitial: 'ca-app-pub-6258370794686593/7204729579',
  rewardvideo: 'ca-app-pub-6258370794686593/4275686972',
}
Object.freeze(admobIds)
new App(admobIds)

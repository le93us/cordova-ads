# `cordova` ads

**important!**

Necessary `cordova-plugin-admpb-free` requires
`cordova platform add ~6.4.0` command to be executed,
[proof](https://github.com/ratson/cordova-plugin-admob-free/issues/168) .

## AdMob Free Plugin

* [repo](https://github.com/ratson/cordova-plugin-admob-free)
* [API docs](https://ratson.github.io/cordova-plugin-admob-free/variable/index.html)

```sh
cordova platform add ~6.4.0
cordova plugin add cordova-plugin-admob-free --save
```

To install necessary plugins and engines from `config.js` and `package.json`

```sh
cordova serve
```

## Debug

```sh
cordova run
```

After app has been launched open in `chrome` browser `chrome://inspect` and
click on `Cordova AdMob Ads Demo`.

Here is a developer's console.
